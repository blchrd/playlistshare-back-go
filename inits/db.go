package inits

import (
	"os"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var DB *gorm.DB

func DBInit(params ...string) {
	db_path := os.Getenv("DB_PATH")
	if len(params) >= 1 {
		db_path = params[0]
	}

	database, err := gorm.Open(sqlite.Open(db_path), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}

	DB = database
}
