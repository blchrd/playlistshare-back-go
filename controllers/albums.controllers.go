// controllers/albums.go

package controllers

import (
	"encoding/json"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"blchrd/playlistshare/inits"
	"blchrd/playlistshare/models"
	"blchrd/playlistshare/resources"

	"github.com/gin-gonic/gin"
	"golang.org/x/net/html"
)

type BandcampDetails struct {
	ItemType string `json:"type"`
	ItemId   string `json:"id"`
	Artist   string `json:"artist"`
	Title    string `json:"title"`
}

// @BasePath /api/v1

// GetAlbums godoc
// @Summary Get albums list
// @Schemes
// @Description Get the albums list, paginated
// @Tags album
// @Produce json
// @Success 200 {object} []resources.PaginatedAlbumResources
// @Router /albums [get]
func FindAlbums(context *gin.Context) {
	var err error
	page := 1
	perPage := 5

	//handle the sorting parameter
	urlParams := context.Request.URL.Query()

	// Pagination
	if urlParams.Has("page") {
		page, err = strconv.Atoi(urlParams.Get("page"))
		if err != nil {
			context.JSON(http.StatusInternalServerError, gin.H{"error": err.Error})
			return
		}
	}
	if urlParams.Has("perPage") {
		perPage, err = strconv.Atoi(urlParams.Get("perPage"))
		if err != nil {
			context.JSON(http.StatusInternalServerError, gin.H{"error": err.Error})
			return
		}
	}

	query := inits.DB.Preload("Genres")

	// Parsing all the filter
	//private
	if !strings.HasPrefix(context.Request.URL.Path, "/api/v1/public") {
		query = query.Where("IsPrivate = false")
	} else if urlParams.Has("private") {
		query = query.Where("IsPrivate = ?", urlParams["private"][0])
	}
	//listened
	if urlParams.Has("listened") {
		query = query.Where("Listened = ?", urlParams["listened"][0])
	}
	//rating
	if urlParams.Has("rating") {
		query = query.Where("Note = ?", urlParams["rating"][0])
	}
	//search artist
	if urlParams.Has("artist") {
		query = query.Where("Artist LIKE ?", "%"+urlParams["artist"][0]+"%")
	}
	//search title
	if urlParams.Has("title") {
		query = query.Where("Title LIKE ?", "%"+urlParams["title"][0]+"%")
	}
	//search text
	if urlParams.Has("search") {
		query = query.Where(
			"(Artist LIKE ? OR Title LIKE ? OR Comment LIKE ?)",
			"%"+urlParams["search"][0]+"%",
			"%"+urlParams["search"][0]+"%",
			"%"+urlParams["search"][0]+"%")
	}
	//genre
	if urlParams.Has("genre") {
		genresFilters := strings.Split(urlParams["genre"][0], ",")
		for _, genreFilter := range genresFilters {
			query = query.Where(`albums.id in (
				select ag.albums_id 
				from albums_genres ag join genres g on ag.genres_id = g.id 
				and g.genre = ?)`, genreFilter)
		}
	}

	//sort
	if urlParams.Has("sort") {
		query = query.Distinct().Order(urlParams["sort"][0] + " desc")
	} else {
		query = query.Distinct().Order("ListeningDate desc, created_at desc")

	}

	var albums []models.Albums
	var albumsResource []resources.AlbumResource

	query.Find(&albums)
	for _, album := range albums {
		albumsResource = append(albumsResource, resources.AlbumToResource(album))
	}

	context.JSON(http.StatusOK, resources.PaginateAlbumResources(albumsResource, page, perPage))
}

// GetAlbum godoc
// @Summary Get one album
// @Schemes
// @Description Get one album
// @Tags album
// @Produce json
// @Success 200 {object} resources.AlbumResource
// @Router /albums/:id [get]
func FindAlbum(context *gin.Context) {
	var album models.Albums

	result := inits.DB.First(&album, context.Param("id"))
	if result.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": result.Error})
		return
	}

	inits.DB.Preload("Genres").Find(&album)
	if strings.HasPrefix(context.Request.URL.Path, "/api/v1/public") {
		if album.IsPrivate {
			context.JSON(http.StatusNotFound, gin.H{"error": "album not found"})
			return
		}
	}

	context.JSON(http.StatusOK, gin.H{"data": resources.AlbumToResource(album)})
}

// PostAlbum godoc
// @Summary Create one album
// @Schemes
// @Description Create one album
// @Tags album
// @Accept json
// @Produce json
// @Success 200 {object} resources.AlbumResource
// @Router /albums [post]
func CreateAlbum(context *gin.Context) {
	album := getAlbumFromContext(context)
	bcDetails := getBandcampDetails(album.Url)

	album.BandcampItemType = &bcDetails.ItemType
	album.BandcampItemId = &bcDetails.ItemId

	result := inits.DB.Create(&album)
	if result.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": result.Error})
		return
	}

	context.JSON(http.StatusCreated, gin.H{"data": resources.AlbumToResource(album)})
}

// DeleteAlbum godoc
// @Summary Delete album
// @Schemes
// @Description Delete album
// @Tags album
// @Accept json
// @Produce json
// @Success 200 {string} album has been deleted successfully
// @Router /albums/:id [delete]
func DeleteAlbum(context *gin.Context) {
	id := context.Param("id")
	inits.DB.Delete(&models.Albums{}, id)
	context.JSON(http.StatusOK, gin.H{"data": "album has been deleted successfully"})
}

// UpdateAlbum godoc
// @Summary Update an album
// @Schemes
// @Description Update an album
// @Tags album
// @Accept json
// @Produce json
// @Success 200 {object} resources.AlbumResource
// @Router /albums/:id [put]
func UpdateAlbum(context *gin.Context) {
	updatedAlbum := getAlbumFromContext(context)
	var album models.Albums

	result := inits.DB.First(&album, context.Param("id"))
	if result.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": result.Error})
		return
	}

	inits.DB.Model(&album).Association("Genres").Clear()
	album.Genres = updatedAlbum.Genres

	inits.DB.Model(&album).Updates(&updatedAlbum)
	context.JSON(http.StatusOK, gin.H{"data": resources.AlbumToResource(album)})
}

// GetBCDetails godoc
// @Summary Get the details for a bandcamp URL
// @Schemes
// @Description Get the details for a bandcamp URL
// @Tags album
// @Accept json
// @Produce json
// @Success 200 {object} BandcampDetails
// @Router /bc-details [get]
func GetBCDetails(context *gin.Context) {
	bcDetails := BandcampDetails{
		ItemType: "",
		ItemId:   "",
		Artist:   "",
		Title:    "",
	}
	urlParams := context.Request.URL.Query()
	if urlParams.Has("url") {
		bcDetails = getBandcampDetails(urlParams["url"][0])

	}

	context.JSON(http.StatusOK, gin.H{"data": bcDetails})
}

func getBandcampDetails(url string) BandcampDetails {
	bcDetails := BandcampDetails{
		ItemType: "",
		ItemId:   "",
		Artist:   "",
		Title:    "",
	}

	if url != "" {
		bcDetails.ItemType = "url"
		regexBc := regexp.MustCompile(`/(https?:\/\/)?([\d|\w]+)\.bandcamp\.com\/?.*/`)

		if regexBc.MatchString(url) {
			response, err := http.Get(url)
			if err != nil {
				return bcDetails
			}

			defer response.Body.Close()
			tokens := html.NewTokenizer(response.Body)

			artistAndTitleOk := false
			itemTypeAndIdOk := false
			for !artistAndTitleOk || !itemTypeAndIdOk {
				tt := tokens.Next()

				switch {
				case tt == html.ErrorToken:
					return bcDetails
				case tt == html.StartTagToken:
					t := tokens.Token()
					if t.Data == "meta" {
						if t.Attr[0].Val == "title" {
							completeTitle := strings.Split(t.Attr[1].Val, ", by")
							bcDetails.Artist = strings.Trim(completeTitle[len(completeTitle)-1], " ")
							title := ""
							for i := 0; i < len(completeTitle)-1; i++ {
								title += strings.Trim(completeTitle[i], " ") + " "
							}
							bcDetails.Title = strings.Trim(title, " ")

							artistAndTitleOk = true
						} else if t.Attr[0].Val == "bc-page-properties" {
							var properties struct {
								ItemType string `json:"item_type"`
								ItemId   int    `json:"item_id"`
								MiscProp int    `json:"tralbum_page_version"`
							}
							json.Unmarshal([]byte(t.Attr[1].Val), &properties)

							bcDetails.ItemId = strconv.Itoa(properties.ItemId)
							if properties.ItemType == "a" {
								bcDetails.ItemType = "album"
							} else {
								bcDetails.ItemType = "track"
							}

							itemTypeAndIdOk = true
						}
					}
				}
			}
		}
	}

	return bcDetails
}

func getAlbumFromContext(context *gin.Context) models.Albums {
	var body struct {
		Artist        string
		Title         string
		Genre         []string
		Url           string
		Note          int
		Listened      bool
		ListeningDate *time.Time
		Comment       *string
		IsPrivate     bool
	}

	context.BindJSON(&body)

	album := models.Albums{
		Title:         body.Title,
		Artist:        body.Artist,
		Url:           body.Url,
		Note:          body.Note,
		Listened:      body.Listened,
		ListeningDate: body.ListeningDate,
		Comment:       body.Comment,
		IsPrivate:     body.IsPrivate,
	}
	for _, genre := range body.Genre {
		var dbGenre models.Genres
		inits.DB.FirstOrCreate(&dbGenre, models.Genres{Genre: genre})
		album.Genres = append(album.Genres, &dbGenre)
	}

	return album
}
