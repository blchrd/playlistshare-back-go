package models

import (
	"time"
)

type Users struct {
	ID        uint
	Name      string
	Email     string `gorm:"unique"`
	Password  string
	CreatedAt *time.Time
	UpdatedAt *time.Time
}
