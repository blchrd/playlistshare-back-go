package resources

import (
	"blchrd/playlistshare/models"
	"time"
)

type AlbumResource struct {
	ID               uint       `json:"id"`
	Artist           string     `json:"artist"`
	Title            string     `json:"title"`
	Genre            []string   `json:"genre"`
	Url              string     `json:"url"`
	Note             int        `json:"note"`
	Listened         bool       `json:"listened"`
	ListeningDate    *time.Time `json:"listeningDate"`
	Comment          *string    `json:"comment"`
	BandcampItemId   *string    `json:"bandcampItemId"`
	BandcampItemType *string    `json:"bandcampItemType"`
	IsPrivate        bool       `json:"isPrivate"`
}

type PaginatedAlbumResources struct {
	Data  []AlbumResource `json:"data"`
	Links struct {
		First int  `json:"first"`
		Last  int  `json:"last"`
		Prev  *int `json:"prev"`
		Next  *int `json:"next"`
	} `json:"links"`
	Meta struct {
		CurrentPage int `json:"current_page"`
		LastPage    int `json:"last_page"`
	} `json:"meta"`
}

func AlbumToResource(album models.Albums) AlbumResource {
	albumResource := AlbumResource{
		ID:               album.ID,
		Artist:           album.Artist,
		Title:            album.Title,
		Url:              album.Url,
		Note:             album.Note,
		Listened:         album.Listened,
		ListeningDate:    album.ListeningDate,
		Comment:          album.Comment,
		BandcampItemId:   album.BandcampItemId,
		BandcampItemType: album.BandcampItemType,
		IsPrivate:        album.IsPrivate,
	}

	for _, genre := range album.Genres {
		albumResource.Genre = append(albumResource.Genre, genre.Genre)
	}

	return albumResource
}

func PaginateAlbumResources(albumResources []AlbumResource, currentPage int, perPage int) PaginatedAlbumResources {
	var paginatedAlbumResources PaginatedAlbumResources

	// calculate start and end of the current page
	startIndex := (currentPage - 1) * perPage
	endIndex := (startIndex + perPage)
	if endIndex > len(albumResources) {
		endIndex = len(albumResources)
	}

	// remove useless album from the data
	paginatedAlbumResources.Data = albumResources[startIndex:endIndex]

	// calculate number of page
	lastPage := len(albumResources) / perPage
	if lastPage == 0 {
		lastPage = 1
	}

	// fill the links structure
	paginatedAlbumResources.Links.First = 1
	if currentPage == 1 {
		paginatedAlbumResources.Links.Prev = nil
	} else {
		previousPage := currentPage - 1
		paginatedAlbumResources.Links.Prev = &previousPage
	}
	if currentPage == lastPage {
		paginatedAlbumResources.Links.Next = nil
	} else {
		nextPage := currentPage + 1
		paginatedAlbumResources.Links.Next = &nextPage
	}
	paginatedAlbumResources.Links.Last = lastPage

	// fill the meta structure
	paginatedAlbumResources.Meta.CurrentPage = currentPage
	paginatedAlbumResources.Meta.LastPage = lastPage

	return paginatedAlbumResources
}
